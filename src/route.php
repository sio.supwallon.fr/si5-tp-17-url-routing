<?php
namespace SI5_TP_17;

use SI5_TP_17\controllers\ErrorController;
use SI5_TP_17\controllers\HomeController;
use SI5_TP_17\controllers\PersonneController;
use SI5_TP_17\kernel\Route;
use SI5_TP_17\kernel\Router;

$router = new Router();
$router->addRoute(new Route("/", HomeController::class));
$router->addRoute(new Route("/home", HomeController::class));
$router->addRoute(new Route("/personnes", PersonneController::class));
$router->addRoute(new Route("/personne/{id}", PersonneController::class));
$router->addRoute(new Route("{*}", ErrorController::class));

$route = $router->findRoute();
$route->execute();
