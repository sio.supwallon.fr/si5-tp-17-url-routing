<?php
namespace SI5_TP_17\view;

use SI5_TP_17\kernel\Router;

class View
{
    private static $_name;
    private static $_params = [];
    private static $_route;

    public static function bindParam($name, $value)
    {
        self::$_params[$name] = $value;
    }

    public static function setTemplate($name)
    {
        self::$_name = $name;
    }

    public static function setRoute($route)
    {
        self::$_route = $route;
    }

    public static function display()
    {
        foreach(self::$_params as $name => $value)
        {
            $$name = $value;
        }
        $router = new Router();
        $basePath = $router->getBasePath();


        $template = self::$_name . ".html.php";
        $script = self::$_name . ".js";

        if(isset(self::$_route)) {
            $route = self::$_route;
        }

        require_once "src/view/templates/index.html.php";
    }
}