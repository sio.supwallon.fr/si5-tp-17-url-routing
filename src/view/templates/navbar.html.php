<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="<?= $basePath; ?>">SI5-TP-17</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item<?= ($route->getUri() == '/' || $route->getUri() == '/home')?" active":""; ?>">
        <a class="nav-link" href="<?= $basePath; ?>/home">Home</a>
      </li>
      <li class="nav-item<?= ($route->getUri() == '/personnes')?" active":""; ?>">
        <a class="nav-link" href="<?= $basePath; ?>/personnes">Personnes</a>
      </li>
    </ul>
  </div>
</nav>