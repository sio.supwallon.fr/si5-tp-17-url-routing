<h1>Liste des personnes</h1>
<ul>
<?php foreach($personnes as $personne): ?>
    <li><a href="<?= $basePath; ?>/personne/<?= $personne->id; ?>">
        <?= $personne->nom; ?> <?= $personne->prenom; ?>
    </a></li>
<?php endforeach; ?>
</ul>
