<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>SI5-TP-17</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= $basePath; ?>/vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    </head>
    <body>
<?php require 'navbar.html.php'; ?>
        <main class="container">
<?php require $template; ?>
        </main>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?= $basePath; ?>/vendor/components/jquery/jquery.min.js"></script>
        <script src="<?= $basePath; ?>/vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<?php if(file_exists($basePath . "/src/scripts/" . $script)): ?>        
        <script src="<?= $basePath; ?>/src/scripts/<?= $script ?>"></script>
<?php endif; ?>        
    </body>
</html>