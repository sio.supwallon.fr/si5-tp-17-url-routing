<?php
namespace SI5_TP_17\model\database;

use SI5_TP_17\model\classes\Personne;
use SI5_TP_17\view\View;
use PDO;

class PersonneDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * FROM `personne` WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            View::setTemplate('error-db-statement');
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Personne::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * FROM `personne`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            View::setTemplate('error-db-statement');
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Personne::class);

        $array = $sth->fetchAll();

        return $array;
    }
}