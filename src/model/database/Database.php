<?php
namespace SI5_TP_17\model\database;

use SI5_TP_17\view\View;
use PDO;
use PDOException;

class Database
{
    private static $_driver   = "mysql";
    private static $_host     = "host=192.168.1.11;";
    private static $_port     = "port=3306;";
    private static $_charset  = "charset=utf8;";
    private static $_database = "dbname=si5-tp-17;";
    private static $_user     = "si5-tp-17";
    private static $_password = "si5-tp-17";

    private static $_dsn;
    private static $_connection;
    
    public static function get_connection()
    {
        if(!isset(self::$_connection)) {
            self::$_dsn = self::$_driver . ":" 
                        . self::$_host 
                        . self::$_port 
                        . self::$_charset 
                        . self::$_database;
            try {
                self::$_connection = new PDO(self::$_dsn, self::$_user, self::$_password);
            } catch (PDOException $e) {
                echo $e->getMessage();
                var_dump($e->errorInfo);
                View::setTemplate('error-db-connection');
                View::display();
                die();
            }
        }
        return self::$_connection;
    }
}