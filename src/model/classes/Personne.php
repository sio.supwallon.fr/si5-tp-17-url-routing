<?php
namespace SI5_TP_17\model\classes;
use SI5_TP_17\model\database\PersonneDao;

class Personne extends Entity
{
    protected $id;
    protected function get_id() { return $this->id; }
    protected function set_id($value) { $this->id = $value; }

    protected $nom;
    protected function get_nom() { return $this->nom; }
    protected function set_nom($value) { $this->nom = $value; }

    protected $prenom;
    protected function get_prenom() { return $this->prenom; }
    protected function set_prenom($value) { $this->prenom = $value; }

    public static function getAll() { return PersonneDao::getAll(); }
    public static function get($id) { return PersonneDao::get($id); }
}