<?php
namespace SI5_TP_17\controllers;

use SI5_TP_17\kernel\Route;
use SI5_TP_17\kernel\Router;
use SI5_TP_17\view\View;

class HomeController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/", HomeController::class, "default_action"));
        $router->addRoute(new Route("/home", HomeController::class, "default_action"));
        $router->addRoute(new Route("{*}", ErrorController::class, "error_404_action"));
        
        $route = $router->findRoute();
        View::setRoute($route);
        $route->execute();
    }

    public static function default_action()
    {
        View::setTemplate('home');
        View::display();
    }
}