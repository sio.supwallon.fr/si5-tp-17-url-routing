<?php
namespace SI5_TP_17\controllers;

use SI5_TP_17\kernel\Route;
use SI5_TP_17\kernel\Router;
use SI5_TP_17\model\classes\Personne;
use SI5_TP_17\view\View;

class PersonneController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/personnes", PersonneController::class, "list_action"));
        $router->addRoute(new Route("/personne/{id}", PersonneController::class, "details_action"));
        $router->addRoute(new Route("{*}", ErrorController::class, "error_404_action"));
        
        $route = $router->findRoute();
        View::setRoute($route);
        $route->execute();
    }

    public static function list_action()
    {
        $personnes = Personne::getAll();

        View::setTemplate('personne_list');
        View::bindParam("personnes", $personnes);
        View::display();
    }

    public static function details_action($id)
    {
        $personne = Personne::get($id);

        View::setTemplate('personne_details');
        View::bindParam("personne", $personne);
        View::display();
    }
}